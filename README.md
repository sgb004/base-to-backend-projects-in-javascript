# Base to backend projects in Javascript

Run:

rm -r .git

git init

git remote add {branch_name} {url_repository}

add your .prettierrc.js file (nano .prettierrc.js) with your rules, as: module.exports = { semi: true, singleQuote: true, useTabs: true };

echo "module.exports = { semi: true, singleQuote: true, useTabs: true };" > .prettierrc.js

rm README.md

npm update
